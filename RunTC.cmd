@echo off

if not defined timeout set timeout=300000
if not defined dataBaseLogin set dataBaseLogin=default
if not defined dataBaseServer set dataBaseServer=default
if not defined dataBasePassword set dataBasePassword=default
if not defined browser set browser=default
if not defined testRunType set testRunType=Api
if not defined tags set tags="TestCategory=patch_smoke & TestCategory=smoke | TestCategory=SQL"

dotnet test ATFramework\ATFramework.sln --filter:%tags% ^
-- TestRunParameters.Parameter(name=\"dataBaseLogin\", value=\"%dataBaseLogin%\") ^
TestRunParameters.Parameter(name=\"dataBasePassword\", value=\"%dataBasePassword%\") ^
TestRunParameters.Parameter(name=\"dataBaseServer\", value=\"%dataBaseServer%\") ^
TestRunParameters.Parameter(name=\"browser\", value=\"%browser%\") ^
TestRunParameters.Parameter(name=\"testRunType\", value=\"%testRunType%\")