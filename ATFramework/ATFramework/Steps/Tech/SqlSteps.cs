﻿using System.Collections.Generic;
using System.Linq;
using ATFramework.Core;
using ATFramework.Core.WorkWithCore;
using HelperProject.Extensions;
using Should;
using TechTalk.SpecFlow;

namespace ATFramework.Steps.Tech
{
    [Binding]
    public class SqlSteps
    {
        [Given(@"Table '(.*)' contains row \(Sql\)")]
        [When(@"I insert row in table '(.*)' \(Sql\)")]
        public void GivenTableContainsRow(string tableName, Table table)
        {
            for (var i = 0; i < table.RowCount; i++)
            {
                var data = table.Header.ToList().ToDictionary(header => header, header => table.Rows[i][header]);
                var insertData = table.Header.ToList().ToDictionary(header => header, header => table.Rows[i][header]);

                if (!DataBase.DoesDbHaveRow(tableName, data))
                    DataBase.AddRow(tableName, insertData);
            }
        }

        [Then(@"Table '(.*)' contains value \(Sql\)")]
        public void ThenTableContainsValue(string tableName, Table table)
        {
            for (var i = 0; i < table.RowCount; i++)
            {
                var data = table.Header.ToList().ToDictionary(header => header, header => table.Rows[i][header]);

                DataBase.DoesDbHaveValue(tableName, data).ShouldBeTrue();
            }
        }

        [Then(@"Table '(.*)' (does not )?contains? row \(Sql\)")]
        public void ThenTableContainsRow(string tableName, string status, Table table)
        {
            var expectedStatus = status.IsEmpty();

            for (var i = 0; i < table.RowCount; i++)
            {
                var data = table.Header.ToList().ToDictionary(header => header, header => table.Rows[i][header]);

                DataBase.DoesDbHaveRow(tableName, data).ShouldEqual(expectedStatus);
            }
        }

        [When(@"I delete row in table '(.*)' \(Sql\)")]
        [Given(@"Row in table '(.*)' is deleted \(Sql\)")]
        public void WhenIDeleteRowInTableSql(string tableName, Table table)
        {
            for (var i = 0; i < table.RowCount; i++)
            {
                var data = table.Header.ToList().ToDictionary(header => header, header => table.Rows[i][header]);

                DataBase.DeleteRow(tableName, data);
            }
        }

        [Given(@"Row at table '(.*)' is updated \(Sql\)")]
        public void GivenRowAtTableAreUpdatedSql(string tableName, Table table)
        {
            for (var i = 0; i < table.RowCount; i++)
                DataBase.UpdateRow(tableName, new Dictionary<string, string>
                    {{table.Rows[i]["Set Column"], table.Rows[i]["Set Value"]}}, new Dictionary<string, string>
                    {{table.Rows[i]["Where Column"], table.Rows[i]["Where Value"]}});
        }

        [Then(@"Table '(.*)' contains row found by '(.*)' like '(.*)' \(Sql\)")]
        public void ThenTableContainsRowFoundByLike(string tableName, string columnName, string columnValue, Table table)
        {
            var actualRows = DataBase.GetInfoAtDataBase(tableName, $"* FROM {tableName} WHERE [{columnName}] like '{columnValue}'");

            foreach (var expectedRow in table.Rows)
            {
                var expectedDictionaryRow = table.Header.ToList().ToDictionary(header => header, header => expectedRow[header]);
                var result = actualRows
                    .Select(actualRow => expectedDictionaryRow
                    .Aggregate(true, (current, expCell) => current && actualRow[expCell.Key].CompareStringWithDelta(expCell.Value)))
                    .Any(res => res);

                result.ShouldBeTrue();
            }
        }
    }
}
