﻿using ATFramework.Core.Constants;
using HelperProject.Context;
using HelperProject.Helpers;
using HelperProject.Logging;
using Should;
using System;
using TechTalk.SpecFlow;
using TempMail.Integration.TempMail;
using TempMailModel = TempMail.Integration.TempMail.TempMail;

namespace ATFramework.Steps.Tech
{
    [Binding]
    public class TempMailSteps
    {
        [Given(@"Temp mail box is created \(Tech\)")]
        public void GivenTempMailBoxWasCreatedTech()
        {
            var tempMail = TempMailFactory.GetTempMail(SupportMethods.GetUniqueValue());
            ScenarioDictionary.Current["tempMailBox"] = tempMail;
            Logger.Step($"Temp mail box {tempMail.GetEmail()} was created");
        }

        [Given(@"Temporary mail box is empty \(Tech\)")]
        public static void GivenLeadHasNoMailOnHisBoxTech()
        {
            var tempMail = ScenarioDictionary.Get<TempMailModel>("tempMailBox");
            tempMail.ClearAllEmailsWithWaiting(30);
        }

        [Then(@"The email was received to email \(Tech\)")]
        public static void ThenTheEmailWasReceivedToEmailTech()
        {
            ScenarioDictionary.Current[Context.UnreadMails] = ((TempMailModel)ScenarioDictionary.Current[Context.TempMailBox]).GetAllEmailsWithWaiting();
        }

        [Then(@"User has '(\d*)' unread emails? \(Tech\)")]
        public static void ThenUserHasUnreadEmailsTech(int unreadEmails) =>
            ScenarioDictionary
                .Get<TempMailModel>(Context.TempMailBox)
                .GetAllEmailsWithWaiting(expectedEmailsCount: unreadEmails).Count
                .ShouldEqual(unreadEmails);

        [Then(@"The email about '(.*)' was received to email \(Tech\)")]
        public static void ThenTheEmailAboutChangedPasswordWasReceivedToTempEmailTech(string text)
        {
            var tempMail = (TempMailModel)ScenarioDictionary.Current[Context.TempMailBox];
            var unreadMails = tempMail.GetAllEmailsWithWaiting();
            ScenarioDictionary.Current[Context.UnreadMails] = unreadMails;

            switch (text)
            {
                case "registration":
                    unreadMails[0].Subject.Contains("Your Newbook Client Account").ShouldBeTrue();
                    break;
                default:
                    throw new Exception($"Email about {text} cannot be checked.");
            }
        }
    }
}
