﻿using HelperProject.Extensions;
using HelperProject.Helpers;
using Should;
using System;
using System.Linq;
using TechTalk.SpecFlow;

namespace ATFramework.Steps.Tech
{
    [Binding]
    public class FileSteps
    {
        [Then(@"File '(.*)' is downloaded \(Tech\)")]
        public void ThenFileIsDownloadedTech(string fileName)
        {
            FileHelper.IsDownloadedFileExistWithWaiting(fileName).ShouldBeTrue();
            FileHelper.IsFileDownloaded().ShouldBeTrue();
        }

        [Then(@"File is (not )?downloaded \(Tech\)")]
        public void ThenFileIsNotDownloadTech(string status) =>
                FileHelper.IsAnyFileDownloaded().ShouldEqual(status.IsEmpty());

        [Then(@"Downloaded CSV file with name '(.*)' (not contains|contains|<.*>) row \(Tech\)")]
        public void ThenDownloadedCsvFileWithNameContainsRowTech(string fileName, string status, Table table)
        {
            var csvStatus = status.Equals("contains");

            if (FileHelper.IsDownloadedFileExistWithWaiting(fileName))
            {
                var actualFileRows = FileHelper.ReadingFile(FileHelper.GetPathToDownloadedFile());

                foreach (var row in table.Rows)
                {
                    var expectedRow = string.Join(",", row.Values).TrimEnd(',');
                    actualFileRows.Any(r => r.Equals(expectedRow)).ShouldEqual(csvStatus);
                }
            }
            else
                throw new Exception("File was not downloaded!");
        }
    }
}
