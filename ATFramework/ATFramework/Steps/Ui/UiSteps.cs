﻿using ATFramework.Core.POM.Models.SingUp;
using ATFramework.Core.POM.Pages;
using ATFramework.Core.POM.Pages.SingUp;
using ATFramework.Core.WorkWithCore;
using Should;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using static HelperProject.SeleniumWrapper;

namespace ATFramework.Steps.Ui
{
    [Binding]
    public class UiSteps
    {
        #region Given

        [Given(@"Page '(.*)' is open on NewBook site \(Gui\)")]
        public void GivenPageIsOpenOnNewBookSiteGui(string pageName)
        {
            GetDriver().Navigate().GoToUrl(EnvParams.ProjectUrl());
            WaitElementVisible(HeaderL.SignUpBtn);

            switch (pageName.Replace(" ", "").ToUpperInvariant())
            {
                case "SINGUP":
                    Header.ClickSignUpBtn();
                    break;
                default:
                    throw new Exception($"Page {pageName} is not implemented.");
            }
        }

        #endregion

        #region When
        
        [When(@"I fill data of user on '(.*)' page \(Gui\)")]
        public void WhenIFillFieldOnPageGui(string pageName, Table table)
        {
            switch (pageName.Replace(" ", "").ToUpperInvariant())
            {
                case "FIRSTSINGUP":
                    SingUpPage.FillFirstPage(table.CreateInstance<SingUpFirstPageModel>());
                    break;
                case "SECONDSINGUP":
                    SingUpPage.FillSecondPage(table.CreateInstance<SingUpSecondPageModel>());
                    break;
                default:
                    throw new Exception($"Page {pageName} is not implemented.");
            }
        }

        #endregion

        #region Then

        [Then(@"Authorization is successful \(Gui\)")]
        public void ThenAuthorizationIsSuccessfulGui() =>
            IsElementVisible(HeaderL.SignUpBtn).ShouldBeFalse();

        [Then(@"Validation error '(.*)' message is displayed \(Gui\)")]
        public void ThenValidationErrorMessageIsDisplayedGui(string text) =>
            SingUpPage.GetErrorText().ShouldEqual(text);

        #endregion
    }
}