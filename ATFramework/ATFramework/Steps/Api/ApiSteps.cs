﻿using ATFramework.Core.Api.Methods;
using ATFramework.Core.Api.Models;
using HelperProject.Logging;
using Newtonsoft.Json.Linq;
using TechTalk.SpecFlow;

namespace ATFramework.Steps.Api
{
    [Binding]
    public class ApiSteps
    {
        [Given(@"Demo user exists \(Api\)")]
        [When(@"I create demo user \(Api\)")]
        public void WhenICreateDemoUserApi(Table table)
        {
            Logger.Step("Demo user creating ...");

            foreach (var row in table.Rows)
            {
                var firstPageModel = new SingUpFirstPageModel()
                {
                    Email = row["Email"],
                    FirstName = row["First Name"],
                    LastName = row["Last Name"],
                    Password = row["Password"],
                    PhoneNumber = row["Mobile"]
                };

                var responseData = ApiMethods.SendRequestSingUpPost(firstPageModel);

                var secondPageModel = new SingUpSecondPageModel()
                {
                    CompanyName = row["Company Name"],
                    CompanyWebsite = row["Company URL"],
                    LocationName = row["Address"],
                    Industry = row["Industry"]
                };

                var token = JObject.Parse(responseData).SelectToken("token_data.token", true)?.ToString();

                ApiMethods.SendRequestSingUpPatch(secondPageModel, token);

                Logger.Step($"Demo user {row["First Name"]} was created successfully!");
            }
        }
    }
}
