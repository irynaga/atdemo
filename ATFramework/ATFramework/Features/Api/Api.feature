﻿@TC @ApiStepsDemo @Automated_Test
Feature: Api requests in feature files
	Demonstration api steps with BaseRequest class
	And how it can be if use CAS steps

@p1 @smoke
Scenario: Create user on newbook site with Api approach
	Given Temp mail box is created (Tech)
	And Temporary mail box is empty (Tech)
	And Demo user exists (Api)
	| First Name        | Last Name        | Email             | Password         | Confirm Password | Mobile     | Company Name   | Company URL                            | Address         | Industry |
	| unique first name | unique last name | current temp mail | default password | default password | 9990009999 | my new company | https://newbookmodels.com/join/company | Dallas, TX, USA | CPG      |
	Then User has '2' unread email (Tech)
	And The email about 'registration' was received to email (Tech)

@patch_smoke
Scenario: Create user on newbook site with Configurable Api Steps approach
	Given Temp mail box is created (Tech)
	And Temporary mail box is empty (Tech)
	And Variable are created (Tech)
	| Name      | Value             |
	| EMAIL     | current temp mail |
	| FIRSTNAME | unique first name |
	| LASTNAME  | unique last name  |
	| PASSWORD  | default password  |
	When I send POST request with data to https://api.newbookmodels.com/api/v1/auth/client/signup/ (Api)
	"""
	{
		"Body":
				"{
				  \"email\":\"%EMAIL%\",
				  \"first_name\":\"%FIRSTNAME%\",
				  \"last_name\":\"%LASTNAME%\",
				  \"password\":\"%PASSWORD%\",
				  \"phone_number\":\"9990000000\"
				}"
	}
	"""
	Given Variable are created (Tech)
	| Name  | Value                                                                      |
	| TOKEN | value 'token_data.token' from response of 'v1/auth/client/signup/' request |
	When I send PATCH request with data to https://api.newbookmodels.com/api/v1/client/profile/ (Api)
	"""
	{
		"Headers": {
				"content-type": "application/json",
				"authorization": "%TOKEN%"
	},
		"Body":
				"{
				  \"industry\":\"123\",
				  \"location_admin1_code\":\"FL\",
				  \"location_city_name\":\"Tampa\",
				  \"location_latitude\":\"27.950575\",
				  \"location_longitude\":\"-82.4571776\",
				  \"location_name\":\"Tampa, FL, USA\",
				  \"location_timezone\":\"America/New_York\"
				}"
	}
	"""
	Then User has '2' unread email (Tech)
	And The email about 'registration' was received to email (Tech)