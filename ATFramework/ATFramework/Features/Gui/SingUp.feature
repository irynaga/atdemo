﻿@TC @SingUpPage @Automated_Test
Feature: SingUp
	SingUp on newbook site

@p1 @smoke
Scenario: Successful Authorization
	Given Temp mail box is created (Tech)
	And Temporary mail box is empty (Tech)
	And Page 'Sing Up' is open on NewBook site (Gui)
	When I fill data of user on 'first sing up' page (Gui)
	| First Name        | Last Name        | Email             | Password         | Confirm Password | Mobile     |
	| unique first name | unique last name | current temp mail | default password | default password | 9990009999 |
	And I fill data of user on 'second sing up' page (Gui)
	| Company Name | Company URL                                      | Address          | Industry  |
	| My company   | https://search.google.com/search-console/welcome | Area 51, NV, USA | Cosmetics |
	Then Authorization is successful (Gui)
	And User has '2' unread email (Tech)
	And The email about 'registration' was received to email (Tech)

@p2 @regression
Scenario Outline: Authorization with incorrect Address data
	Given Page 'Sing Up' is open on NewBook site (Gui)
	When I fill data of user on 'first sing up' page (Gui)
	| First Name        | Last Name        | Email        | Password         | Confirm Password | Mobile     |
	| unique first name | unique last name | unique email | default password | default password | 9990009999 |
	And I fill data of user on 'second sing up' page (Gui)
	| Company Name | Company URL                                      | Address          |
	| My company   | https://search.google.com/search-console/welcome | Area 51, NV, USA |
	Then Validation error 'Required' message is displayed (Gui)