﻿@TC @SQL @Automated_Test
Feature: SQL
	Demonstration DataBase steps

#For run this kind of step you should set up value in env.runsettings file.
#<Parameter name="dataBaseLogin" value="here_you_login" />
#<Parameter name="dataBasePassword" value="here_you_password" />
#<Parameter name="dataBaseServer" value="55.77.999.9(example server value)" />
#Note! Use column names exactly the same as in the table.
@p1 @smoke
Scenario: Successful work with DB
	Given Table '[MySpaceDB].[Table1]' contains row (Sql)
	| FirstName | LastName | Salary |
	| Maria     | Popova   | 1000   |
	When I insert row in table '[MySpaceDB].[Table1]' (Sql)
	| FirstName | LastName | Salary |
	| Peter     | Petrov   | 5000   |
	And I delete row in table '[MySpaceDB].[Table1]' (Sql)
	| FirstName | LastName | Salary |
	| Maria     | Popova   | 1000   |
	Then Table '[MySpaceDB].[Table1]' contains value (Sql)
	| FirstName | LastName | Salary |
	| Peter     | Petrov   | 5000   |
	And Table '[MySpaceDB].[Table1]' does not contain row (Sql)
	| FirstName | LastName | Salary |
	| Maria     | Popova   | 1000   |