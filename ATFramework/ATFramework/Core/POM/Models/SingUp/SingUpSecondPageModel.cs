﻿namespace ATFramework.Core.POM.Models.SingUp
{
    public class SingUpSecondPageModel
    {
        public string CompanyName { get; set; }
        public string CompanyUrl { get; set; }
        public string Address { get; set; }
        public string Industry { get; set; }
    }
}