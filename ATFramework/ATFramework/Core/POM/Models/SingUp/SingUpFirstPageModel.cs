﻿namespace ATFramework.Core.POM.Models.SingUp
{
    public class SingUpFirstPageModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Mobile { get; set; }
        public string ReferralCode { get; set; }
    }
}