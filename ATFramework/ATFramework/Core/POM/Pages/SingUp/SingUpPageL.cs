﻿using OpenQA.Selenium;

namespace ATFramework.Core.POM.Pages.SingUp
{
    internal static class SingUpFirstPageL
    {
        internal static readonly By FirstName = By.Name("first_name");
        internal static readonly By LastName = By.Name("last_name");
        internal static readonly By Email = By.Name("email");
        internal static readonly By Password = By.Name("password");
        internal static readonly By ConfirmPassword = By.Name("password_confirm");
        internal static readonly By Mobile = By.Name("phone_number");
        internal static readonly By ReferralCode = By.Name("code");
        internal static readonly By NextBtn = By.CssSelector("button[class*='submitButton']");
    }

    internal static class SingUpSecondPageL
    {
        internal static readonly By CompanyName = By.Name("company_name");
        internal static readonly By CompanyUrl = By.Name("company_website");
        internal static readonly By Address = By.Name("location");
        internal static readonly By AddressItem = By.CssSelector("[class*='pac-item']");
        internal static readonly By Industry = By.Name("industry");
        internal static readonly By IndustryDropDown = By.CssSelector("[aria-labelledby*='industry']");
        internal static readonly By FinishBtn = By.CssSelector("button[class*='submitButton']");
        internal static readonly By ErrorText = By.CssSelector("div[class*='FormErrorText'] div");
    }
}