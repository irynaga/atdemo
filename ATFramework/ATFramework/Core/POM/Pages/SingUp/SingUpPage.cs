﻿using ATFramework.Core.POM.Models.SingUp;
using HelperProject;
using HelperProject.Extensions;
using OpenQA.Selenium;
using static HelperProject.SeleniumWrapper;

namespace ATFramework.Core.POM.Pages.SingUp
{
    public static class SingUpPage
    {
        #region Sing Up the first page

        public static void FillFirstPage(SingUpFirstPageModel singUpFirstPageModel)
        {
            foreach (var parameter in singUpFirstPageModel.GetPropertiesAsDictionary())
                SetDataOnFirstPage(parameter.Key, parameter.Value.ToString());
            ClickNextBtn();
        }

        private static void SetDataOnFirstPage(string parameterKey, object parameterValue)
        {
            switch (parameterKey)
            {
                case nameof(SingUpFirstPageModel.FirstName): SetFirstName((string)parameterValue); break;
                case nameof(SingUpFirstPageModel.LastName): SetLastName((string)parameterValue); break;
                case nameof(SingUpFirstPageModel.Email): SetEmail((string)parameterValue); break;
                case nameof(SingUpFirstPageModel.Password): SetPassword((string)parameterValue); break;
                case nameof(SingUpFirstPageModel.ConfirmPassword): SetConfirmPassword((string)parameterValue); break;
                case nameof(SingUpFirstPageModel.Mobile): SetMobile((string)parameterValue); break;
                case nameof(SingUpFirstPageModel.ReferralCode): SetReferralCode((string)parameterValue); break;
            }
        }

        private static void SetFirstName(string name) =>
            ClearAndSendValue(FindElement(SingUpFirstPageL.FirstName), name);

        private static void SetLastName(string name) =>
            ClearAndSendValue(FindElement(SingUpFirstPageL.LastName), name);

        private static void SetEmail(string email) =>
            ClearAndSendValue(FindElement(SingUpFirstPageL.Email), email);

        private static void SetPassword(string password) =>
            ClearAndSendValue(FindElement(SingUpFirstPageL.Password), password);

        private static void SetConfirmPassword(string password) =>
            ClearAndSendValue(FindElement(SingUpFirstPageL.ConfirmPassword), password);

        private static void SetMobile(string phoneNumber) =>
            ClearAndSendValue(FindElement(SingUpFirstPageL.Mobile), phoneNumber);

        private static void SetReferralCode(string code) =>
            ClearAndSendValue(FindElement(SingUpFirstPageL.ReferralCode), code);

        public static void ClickNextBtn() =>
            FindElement(SingUpFirstPageL.NextBtn).Click();

        #endregion

        #region Sing Up the second page

        public static void FillSecondPage(SingUpSecondPageModel singUpSecondPageModel)
        {
            foreach (var parameter in singUpSecondPageModel.GetPropertiesAsDictionary())
                SetDataOnSecondPage(parameter.Key, parameter.Value.ToString());
            ClickFinishBtn();
        }

        private static void SetDataOnSecondPage(string parameterKey, object parameterValue)
        {
            switch (parameterKey)
            {
                case nameof(SingUpSecondPageModel.CompanyName): SetCompanyName((string)parameterValue); break;
                case nameof(SingUpSecondPageModel.CompanyUrl): SetCompanyUrl((string)parameterValue); break;
                case nameof(SingUpSecondPageModel.Address): SetAddress((string)parameterValue); break;
                case nameof(SingUpSecondPageModel.Industry): SetIndustry((string)parameterValue); break;
            }
        }

        private static void SetCompanyName(string name) =>
            ClearAndSendValue(FindElement(SingUpSecondPageL.CompanyName), name);

        private static void SetCompanyUrl(string url) =>
            ClearAndSendValue(FindElement(SingUpSecondPageL.CompanyUrl), url);

        private static void SetAddress(string address)
        {
            ClearAndSendValue(FindElement(SingUpSecondPageL.Address), address);
            FindElement(SingUpSecondPageL.AddressItem).Click();
        }

        private static void SetIndustry(string industry)
        {
            FindElement(SingUpSecondPageL.Industry).Click();
            var locator = $"//div[contains(@aria-labelledby, 'SIGNUP_FIRST_FORM/industry')]//span[contains(text(),'{industry}')]";
            FindElement(By.XPath(locator)).Click();
        }

        public static void ClickFinishBtn() =>
            FindElement(SingUpSecondPageL.FinishBtn).Click();

        public static string GetErrorText() =>
            FindElement(SingUpSecondPageL.ErrorText).Text;

        #endregion
    }
}