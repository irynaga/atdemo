﻿using HelperProject;

namespace ATFramework.Core.POM.Pages
{
    public static class Header
    {
        public static void ClickSignUpBtn() =>
            SeleniumWrapper.FindElement(HeaderL.SignUpBtn).Click();
    }
}