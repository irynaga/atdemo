﻿namespace ATFramework.Core.Constants
{
    public static class Context
    {
        public const string TempMailBox = "tempMailBox"; 
        public const string UnreadMails = "unreadMails";
        public const string WebDriver = "webDriver";
    }
}