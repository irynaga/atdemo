﻿using HelperProject.Helpers;
using NUnit.Framework;
using System.IO;
using System.Linq;

namespace ATFramework.Core.Constants
{
    public static class Constants
    {
        public const string Config = "config.xml";
        public const string DefaultPassword = "12345Qwe@";

        public static readonly string ImplementationType = TestContext.Parameters.Get("testRunType");

        public static string GetPathToSolutionRoot()
        {
            var path = FileHelper.GetPathToSolutionRoot();
            while (!Directory.GetFiles(path, "*.sln", SearchOption.TopDirectoryOnly).Any());

            return path;
        }
    }
}