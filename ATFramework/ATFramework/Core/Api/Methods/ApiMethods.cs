﻿using ATFramework.Core.Api.Models;
using ATFramework.Core.WorkWithCore;
using HelperProject;
using HelperProject.Logging;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ATFramework.Core.Api.Methods
{
    public class ApiMethods
    {
        /// <summary>
        /// Send request sing up POST.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SendRequestSingUpPost(SingUpFirstPageModel data)
        {
            var response = new BaseRequest(EnvParams.ApiUrl(),
                AppUrls.Registration.SingUp(), Method.POST).SendRestRequest(JsonConvert.SerializeObject(data));

            Logger.Step($"Request {EnvParams.ApiUrl()}{AppUrls.Registration.SingUp()} POST was sent successfully.");

            return response.Content;
        }

        /// <summary>
        /// Send request sing up PATCH.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SendRequestSingUpPatch(SingUpSecondPageModel data, string authorization)
        {
            var header = new Dictionary<string, string>
            {
                {"authorization", authorization}
            };

            var response = new BaseRequest(EnvParams.ApiUrl(),
                AppUrls.Registration.SingUpSecondStep(), Method.PATCH).SendRestRequest(data, header);

            Logger.Step($"Request {EnvParams.ApiUrl()}{AppUrls.Registration.SingUpSecondStep()} PATCH was sent successfully.");

            return response.Content;
        }
    }
}
