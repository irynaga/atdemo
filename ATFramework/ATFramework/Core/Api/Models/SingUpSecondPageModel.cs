﻿using Newtonsoft.Json;

namespace ATFramework.Core.Api.Models
{
    public class SingUpSecondPageModel
    {
        [JsonProperty("company_name")]
        public string CompanyName { get; set; }

        [JsonProperty("company_website")]
        public string CompanyWebsite { get; set; }
        
        public string Industry { get; set; }

        [JsonProperty("location_admin1_code")]
        public string LocationAdmin1Code { get; set; }

        [JsonProperty("location_city_name")]
        public string LocationCityName { get; set; }

        [JsonProperty("location_latitude")]
        public string LocationLatitude { get; set; }

        [JsonProperty("location_longitude")]
        public string LocationLongitude { get; set; }

        [JsonProperty("location_name")]
        public string LocationName { get; set; }

        [JsonProperty("location_timezone")]
        public string LocationTimezone { get; set; }
    }
}