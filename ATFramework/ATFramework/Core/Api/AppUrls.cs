﻿namespace ATFramework.Core.Api
{
    internal static class AppUrls
    {
        private static readonly string _rootApi = "/api";

        internal static class Registration
        {
            internal static string SingUp() => $"{_rootApi}/v1/auth/client/signup/";
            internal static string SingUpSecondStep() => $"{_rootApi}/v1/client/profile/";
        }
    }
}