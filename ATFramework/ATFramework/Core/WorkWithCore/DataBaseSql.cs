﻿using Sql.Integration;
using System.Collections.Generic;
using System.Data;

namespace ATFramework.Core.WorkWithCore
{
    public class DataBase
    {
        public static bool DoesDbHaveValue(string field, string value, string tableName)
        {
            var res = false;

            foreach (DataRow dbRow in new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).SelectAll(tableName).Rows)
                if (dbRow[field].ToString().Equals(value))
                {
                    res = true;
                    break;
                }

            return res;
        }

        public static bool DoesDbHaveValue(string tableName, Dictionary<string, string> requestData)
        {
            var res = false;

            foreach (var data in requestData)
                foreach (DataRow dbRow in new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).SelectAll(tableName).Rows)
                    if (dbRow[data.Key].ToString().Equals(data.Value))
                    {
                        res = true;
                        break;
                    }

            return res;
        }

        public static bool DoesDbHaveRow(string tableName, Dictionary<string, string> requestData) =>
            new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).DoesRowExist(tableName, requestData);

        public static void AddRow(string tableName, Dictionary<string, string> requestData) =>
            new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).Insert(tableName, requestData);

        public static void DeleteRow(string tableName, Dictionary<string, string> requestData) =>
            new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).Delete(tableName, requestData);

        public static void UpdateRow(string tableName, Dictionary<string, string> setData, Dictionary<string, string> whereData) =>
            new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).UpdateRow(tableName, setData, whereData);

        public static List<Dictionary<string, string>> GetAllRowsAtTableAsDictionary(string tableName) =>
            new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).GetAllRowsAtTableAsDictionary(tableName);

        public static List<Dictionary<string, string>> GetInfoAtDataBase(string tableName, string request) =>
            new SqlHelper(tableName.Split('.')[0].Trim('[', ']')).GetInfoAtDataBase(request);
    }
}