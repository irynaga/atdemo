﻿using NUnit.Framework;
using System;
using System.IO;
using System.Xml;

namespace ATFramework.Core.WorkWithCore
{
    public class EnvParams
    {
        // Names of parameters.
        private const string Config = "configuration";
        private const string ProjectUrlNode = "projectUrl";
        private const string DefaultUserNode = "defaultUser";
        private const string ApiUrlNode = "apiUrl";

        // Test configurations.
        private static string _browser = TestContext.Parameters.Get("browser");

        private static XmlNode CurrentConfig { get; }

        static EnvParams()
        {
            var configXml = new XmlDocument();

            // Load config xml.
            try
            {
                var path = Constants.Constants.GetPathToSolutionRoot() + Constants.Constants.Config;

                configXml.Load(path);
            }
            catch (Exception e)
            {
                throw new FileLoadException($"Problem with loading config xml - {e.Message}");
            }

            // Gets XmlElement by configuration.
            CurrentConfig = configXml.DocumentElement;
        }

        public static string BrowserName()
        {
            if (_browser == null || _browser.Equals("default"))
                _browser = "chrome";

            return _browser;
        }

        public static string ProjectUrl() =>
            CurrentConfig.SelectSingleNode(Config)?.SelectSingleNode(ProjectUrlNode)?.InnerText;

        public static string ApiUrl() =>
            CurrentConfig.SelectSingleNode(Config)?.SelectSingleNode(ApiUrlNode)?.InnerText;

        /*--- Logins/Passwords ---*/
        public static string DefaultUserLogin()
        {
            return TestContext.Parameters.Exists("defaultUser_login")
                ? TestContext.Parameters.Get("defaultUser_login")
                : CurrentConfig.SelectSingleNode(DefaultUserNode)?.Attributes?["login"].Value;
        }

        public static string DefaultUserPassword()
        {
            return TestContext.Parameters.Exists("defaultUser_password")
                ? TestContext.Parameters.Get("defaultUser_password")
                : CurrentConfig.SelectSingleNode(DefaultUserNode)?.Attributes?["password"].Value;
        }
    }
}