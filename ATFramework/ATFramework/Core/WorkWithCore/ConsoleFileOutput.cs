﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace Core.Core
{
    [TestFixture]
    public class ConsoleFileOutput : TextWriter
    {
        private readonly TextWriter _console;
        private static ConsoleFileOutput _instance;
        private static readonly object _padlock = new object();
        readonly Dictionary<int, Tuple<string, StreamWriter>> _threadsDictionary
            = new Dictionary<int, Tuple<string, StreamWriter>>();

        public override Encoding Encoding { get; } = Encoding.UTF8;

        public static ConsoleFileOutput Instance
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                lock (_padlock)
                    return _instance ??= new ConsoleFileOutput();
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private StreamWriter GetThreadWriter()
        {
            var currentThreadId = Thread.CurrentThread.ManagedThreadId;
            if (!_threadsDictionary.ContainsKey(currentThreadId))
            {
                var path = Path.GetTempPath() + "\\" + Guid.NewGuid() + ".txt";
                var writer = new StreamWriter(path)
                {
                    AutoFlush = true
                };
                _threadsDictionary.Add(currentThreadId, new Tuple<string, StreamWriter>(path, writer));
            }
            return _threadsDictionary[currentThreadId].Item2;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string GetThreadConsoleLog()
        {
            var result = "";
            var currentThreadId = Thread.CurrentThread.ManagedThreadId;
            if (_threadsDictionary.ContainsKey(currentThreadId))
                result = _threadsDictionary[currentThreadId].Item1;

            return result;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private ConsoleFileOutput() =>
            _console = Console.Out;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public override void Write(string value)
        {
            Console.SetOut(_console);
            Console.Write(value);

            Console.SetOut(this);

            var writer = GetThreadWriter();
            writer.Write(value);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public override void WriteLine(string value)
        {
            Console.SetOut(_console);
            Console.WriteLine(value);

            Console.SetOut(this);

            var writer = GetThreadWriter();
            writer.WriteLine(value);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public override void Flush() =>
            GetThreadWriter().Flush();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public override void Close() =>
            GetThreadWriter().Close();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void DisposeThread()
        {
            var writer = GetThreadWriter();
            writer.Flush();
            writer.Close();
            writer.Dispose();
            var currentThreadId = Thread.CurrentThread.ManagedThreadId;
            _threadsDictionary.Remove(currentThreadId);
        }
    }
}