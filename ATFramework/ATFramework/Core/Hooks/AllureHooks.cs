﻿using Allure.Commons;
using ATFramework.Core.Constants;
using HelperProject.Context;
using HelperProject.Logging;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using TechTalk.SpecFlow;

namespace Core.Core.Hooks
{
    [TestFixture]
    [Binding]
    public class AllureHooks : Steps
    {
        readonly ScenarioContext _localContext;

        public AllureHooks(ScenarioContext context) =>
            _localContext = context;

        string _path = "";
        TextWriter _dualOutput;

        [BeforeStep(Order = int.MinValue)]
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AllureBeforeStepHook()
        {
            if (null != _localContext) _dualOutput?.WriteLine("\n" + _localContext.StepContext.StepInfo.Text);
        }

        [AfterStep]
        public void AfterStepScreenshotHook()
        {
            if (FeatureDictionary.Current.ContainsKey(Context.WebDriver)
              && !_localContext.StepContext.StepInfo.Text.ToUpper().Contains("(API)"))
                try
                {
                    if (null != AllureLifecycle.Instance)
                        AllureLifecycle.Instance.AddAttachment(MakeScreenshot());
                }
                catch (Exception ex)
                {
                    Logger.Info(ex.Message);
                }
        }

        [BeforeScenario(Order = int.MinValue)]
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AllureLoggingBeforeHook()
        {
            try
            {
                _dualOutput = ConsoleFileOutput.Instance;
                Console.SetOut(_dualOutput);
            }
            catch (IOException e)
            {
                var errorWriter = Console.Error;
                errorWriter.WriteLine(e.Message);
            }
        }

        [AfterScenario(Order = int.MinValue)]
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AfterScenarioTestStatusHook()
        {
            try
            {
                if (null != _dualOutput)
                {
                    _path = ((ConsoleFileOutput)_dualOutput).GetThreadConsoleLog();
                    ((ConsoleFileOutput)_dualOutput).DisposeThread();
                    if (File.Exists(_path))
                    {
                        AllureLifecycle.Instance.AddAttachment("Console Output : ", "text/xml", File.ReadAllBytes(_path));
                        File.Delete(_path);
                    }
                }

                AllureLifecycle.Instance.UpdateTestCase(tc =>
                {
                    tc.name = TestContext.CurrentContext.Test.Name; //In order to have different tests in report
                    tc.historyId = TestContext.CurrentContext.Test.FullName; // or    Guid.NewGuid().ToString()
                });

                if (((TestResult)_localContext.Values.ElementAt(1))
                    .steps.ElementAt(0)
                    .name.ToLower()
                    .Equals("given  not automated scenario"))
                    AllureLifecycle.Instance.StopTestCase(x => x.status = Status.broken);
                else if (null != _localContext.TestError || TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
                    AllureLifecycle.Instance.StopTestCase(x => x.status = Status.failed);
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
            }
        }

        private string MakeScreenshot()
        {
            var screenshotTempDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\tmp\\";

            if (Directory.Exists(screenshotTempDirectory))
                Directory.Delete(screenshotTempDirectory, true);

            Directory.CreateDirectory(screenshotTempDirectory);

            string screenshotFileName;
            var title = _localContext.StepContext.StepInfo.Text.Replace('\\', '_').Replace('/', '_');
            var dateTime = DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss");

            if ((screenshotTempDirectory + title + dateTime).Length > 255)
                screenshotFileName = (screenshotTempDirectory + title).Substring(0, 255 - dateTime.Length) + dateTime;
            else
                screenshotFileName = screenshotTempDirectory + title + dateTime;

            screenshotFileName = screenshotFileName.Replace(' ', '_').Replace('\'', '_') + ".jpeg";
            var upperTitle = title.ToUpper();

            if (!upperTitle.Contains("(API)")
                && !upperTitle.Contains("(SQL)")
                && FeatureDictionary.TryGetValue<IWebDriver>(Context.WebDriver, out var browser)
                && (upperTitle.Contains("(GUI)") || upperTitle.Contains("(PROD)") || upperTitle.Contains("(UNIVERSAL)")))
            {
                browser.Manage().Window.Maximize();
                var screenshot = ((ITakesScreenshot)browser).GetScreenshot();
                screenshot.SaveAsFile(screenshotFileName, ScreenshotImageFormat.Jpeg);
            }

            return screenshotFileName;
        }
    }
}