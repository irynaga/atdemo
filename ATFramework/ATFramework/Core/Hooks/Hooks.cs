﻿using ATFramework.Core.Constants;
using Castle.Core.Internal;
using HelperProject.Context;
using HelperProject.Helpers;
using HelperProject.Logging;
using OpenQA.Selenium;
using System.Runtime.CompilerServices;
using TechTalk.SpecFlow;
using TempMailModel = TempMail.Integration.TempMail.TempMail;

namespace ATFramework.Core.Hooks
{
    [Binding]
    public sealed class Hooks : TechTalk.SpecFlow.Steps
    {
        private static int _countOfScenario;

        [BeforeScenario]
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void BeforeScenario()
        {
            // Write all tags.
            var featureTags = string.Concat("@", string.Join(" @", FeatureContext.FeatureInfo.Tags)).TrimEnd('@');
            var scenarioTags = string.Concat("@", string.Join(" @", ScenarioContext.ScenarioInfo.Tags)).TrimEnd('@');

            if (!featureTags.IsNullOrEmpty())
                Logger.Info(string.Concat("Feature tags: ", featureTags));
            if (!scenarioTags.IsNullOrEmpty())
                Logger.Info(string.Concat("Scenario tags: ", scenarioTags));
        }

        [AfterScenario(Order = int.MaxValue - 1)]
        public void AfterScenario()
        {
            if (ScenarioDictionary.Current.ContainsKey(Context.TempMailBox))
            {
                var tempMail = (TempMailModel)ScenarioDictionary.Current[Context.TempMailBox];
                tempMail.DeleteTempMail();
            }

            ScenarioDictionary.Current.Clear();

            if (FeatureDictionary.Current.ContainsKey(Context.WebDriver))
                try
                {
                    WebDriverHelper.AfterScenario(FeatureDictionary.Get<IWebDriver>(Context.WebDriver));
                }
                catch {/*ignore*/}
            
            if (FeatureDictionary.Current.ContainsKey(Context.WebDriver))
                WebDriverHelper.CloseWebDriver(FeatureDictionary.Get<IWebDriver>(Context.WebDriver));
            FeatureDictionary.Current.Clear();
        }

        [AfterScenario]
        public void AfterScenarioLoggerCount()
        {
            _countOfScenario++;

            Logger.Info($"Scenario #{_countOfScenario} was run.");
        }
    }
}