﻿using ATFramework.Core.Constants;
using HelperProject.Context;
using HelperProject.Extensions;
using HelperProject.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Bindings;
using TechTalk.SpecFlow.Bindings.Reflection;

namespace ATFramework.Core.Transformations
{
    /// <summary>
    /// Class with rules for transformation values.
    /// </summary>
    [Binding]
    public class ArgumentTransforms
    {
        private readonly ScenarioContext _scenarioContext;

        public ArgumentTransforms(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        /// <summary>
        /// Tech function for transformation data form table.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        [StepArgumentTransformation]
        public Table TableTransformation(Table table)
        {
            var newTable = new Table((string[])table.Header);

            if (_scenarioContext.ScenarioExecutionStatus == ScenarioExecutionStatus.OK)
                foreach (var row in table.Rows)
                {
                    var valuesOfRow = new List<string>();

                    foreach (var value in row)
                    {
                        var converter = _scenarioContext.ScenarioContainer.Resolve<IStepArgumentTypeConverter>();
                        if (value.Value.Contains(',') && value.Value.Length > 1)
                        {
                            var newValue = string.Empty;

                            foreach (var transformString in TransformToListOfString(value.Value))
                                newValue += $"{converter.Convert(transformString, new RuntimeBindingType(typeof(string)), CultureInfo.CurrentCulture)}, ";

                            valuesOfRow.Add(newValue.TrimEnd().TrimEnd(','));
                        }
                        else
                            valuesOfRow.Add((string)converter.Convert(value.Value, new RuntimeBindingType(typeof(string)), CultureInfo.CurrentCulture));
                    }

                    newTable.AddRow(valuesOfRow.ToArray());
                }

            return newTable;
        }

        /// <summary>
        /// The method for using created unique value with another static string.
        /// Example: created unique guid = "444221" and need to get value "id is 4442211 for user".
        /// Use: "id is 'used unique guid' for user".
        /// Before use this transformation, you need to use "unique" transformation.
        /// </summary>
        /// <param name="part1"></param>
        /// <param name="value">If you save a few unique vales, use "used" with number.
        /// Example: "id is 'used unique guid 2' for user".</param>
        /// <param name="part2"></param>
        /// <returns></returns>
        [StepArgumentTransformation(@"([\s\S]*)'used unique(.*)'([\s\S]*)")]
        public string UsedUniqueTransformWithData(string part1, string value, string part2)
        {
            return $"{part1}{ScenarioDictionary.Get<string>($"unique{value.ToLower()}")}{part2}";
        }

        /// <summary>
        /// The method for using created unique value.
        /// Example: created unique guid = "444221" and need to get it.
        /// Use: "used unique guid".
        /// Before use this transformation, you need to use "unique" transformation.
        /// </summary>
        /// <param name="value">If you save a few unique vales, use "used" with number.
        /// Example: "used unique guid 2".</param>
        /// <returns></returns>
        [StepArgumentTransformation(@"used unique (.*)")]
        public string UsedUniqueTransform(string value)
        {
            return ScenarioDictionary.Get<string>($"unique {value.ToLower()}");
        }

        /// <summary>
        /// The method for using created value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [StepArgumentTransformation(@"used (?!unique)(.*)")]
        public string UsedTransform(string value)
        {
            return ScenarioDictionary.Get<string>(value.ToLower());
        }

        /// <summary>
        /// Generates unique value and save it for re-use.
        /// </summary>
        /// <param name="part1">Start of sentence.</param>
        /// <param name="value">Parameter for choosing a type of generation.
        /// Example "unique email" generate for you some email with AT mask.
        /// Full list of values you can see in "case" blocks.</param>
        /// <param name="number">Number for marking unique value.
        /// Example you need to create 2 users with different unique email.
        /// Use "unique email 1" and "unique email 2".
        /// 0 - min value, 999 - max value.</param>
        /// <param name="part2">End of sentence.</param>
        /// <returns></returns>
        [StepArgumentTransformation(@"([\s\S]*)unique( \D*)(\d{0,3})([\s\S]*)")]
        public string UniqueTransform(string part1, string value, string number, string part2)
        {
            var result = ("unique " + value).Trim();
            value = value.ToLower().Trim();

            if (ScenarioDictionary.IsKeyExist($"unique {(value + " " + (number.IsEmpty() ? number : (int.Parse(number) - 1).ToString())).Trim()}"))
                Thread.Sleep(10);

            result = value switch
            {
                "email" => (SupportMethods.GetUniqueValue() + "@domain.com").ToLower(),
                "firstname" => StringTransform.GetFirstName(),
                "first name" => StringTransform.GetFirstName(),
                "lastname" => StringTransform.GetLastName(),
                "last name" => StringTransform.GetLastName(),
                "mail first name" => StringTransform.GetFirstName(),
                "mail last name" => StringTransform.GetLastName(),
                "text" => SupportMethods.GetUniqueValue(10),
                "skype" => SupportMethods.GetUniqueString(),
                "int" => SupportMethods.GetUniqueInt(),
                "password" => SupportMethods.GetUniquePassword(15),
                _ => result
            };

            ScenarioDictionary.Add($"unique {value} {number}".Trim(), result);

            return $"{part1}{result}{part2}";
        }

        /// <summary>
        /// Transforms string to list by ',' as separator.
        /// "a, b, c d e, 12d" => list with 4 elements. 'a', 'b', 'c d e', '12d'.
        /// </summary>
        /// <param name="commaSeparatedList"></param>
        /// <returns></returns>
        [StepArgumentTransformation]
        public List<string> TransformToListOfString(string commaSeparatedList) =>
            commaSeparatedList.Split(new[] { ", " }, StringSplitOptions.None).ToList();

        /// <summary>
        /// Gets default value.
        /// Example: default password.
        /// </summary>
        /// <param name="part1">Start of sentence until 'default'.</param>
        /// <param name="value"> Full list of values you can see in "case" blocks.</param>
        /// <param name="part2">End of sentence after 'password'.</param>
        /// <returns></returns>
        [StepArgumentTransformation(@"([\s\S]*)default (password|admin login|target url)([\s\S]*)")]
        public string DefaultTransform(string part1, string value, string part2) =>
            part1 + value.ToLower() switch
            {
                "password" => Constants.Constants.DefaultPassword,
                _ => "default " + value
            } + part2;

        /// <summary>
        /// Gets current data of lead, manager, promo and others.
        /// </summary>
        /// <param name="part1">Start of sentence until 'current'.</param>
        /// <param name="parameter">Full list of parameters you can see in "case" blocks.</param>
        /// <param name="part2">End of sentence until 'lead'.</param>
        /// <returns></returns>
        [StepArgumentTransformation(@"([\s\S]*)current (\w*(?: [I|i]d| first name| last name| country code| full name| name| (?:e)?mail)?)([\s\S]*)")]
        public string CurrentTransform(string part1, string parameter, string part2)
        {
            var result = parameter.ToLower() switch
            {
                "temp mail" => ScenarioDictionary.Get<TempMail.Integration.TempMail.TempMail>(Context.TempMailBox).GetEmail(),
                _ => $"current {parameter}"
            };

            return $"{part1}{result}{part2}";
        }
    }
}