## AT Demo Repository #

Version - 0.1.0

### What is this repository for? ###

 This repository is created to get quick start to the test automation process in your project. You will get a framework with all
 the necessary packages and features to start writing and running your own tests.
  
### How do I get set up? ###

* Summary of set up
  Cloning a repository, creating and connecting with a new remote repository:
  - Clone the "AT Demo Repository" on your device.  
  - Find folder with cloned repository and open git bush or cmd.
  - Use the 'git remote set-url origin (new URL for the remote repository)' command to change remote repository's URL.
  - Use the 'git remote -v' command to make sure your remote repository's URL has changed.
  
  P.S.  You can also change path to the remote repository through SourceTree
  - Open cloned repository in SourceTree.
  - Open repository settings (in the upper right corner).
  - Clik the Edit button.
  - Add new path to remote repository.
    
* Configuration
  All configurations for the framework are already set. To work with the framework, you need to install Visual Studio and SpecFlow extention for VS.
  
  File config.xml  
  - File can store application settings, credentials, URLs, brand/environment names and etc.
  - Configuration files are saved in XML format.
  
  File env.runsettings - stores configurations of specific run like browser where will run test, credentials for DB and etc. You can create several runsetting files.
  (instruction about .runsettings file with a list of possible settings https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/3707535605/A+.runsettings+file+with+a+list+of+possible+settings+and+their+possible+states.)

* Dependencies
  In framework we have integrations with:
   - Helper Project (instruction about HP https://docs.google.com/document/d/1ImlLGLSJ9xG96YCzbTNBENi7_NewpVppVEtQSNmEoOM/edit).
   - FreePhone (instruction about FreePhone https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/1467056142/Virtual+Phone+Integration)  - not integrated yet
   - TempMail (instruction about TempMail https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/edit-v2/3637510278?draftShareId=3d3a0d1e-4af9-4013-9fcb-e632fb7dfb2b).
   - Sql (instruction about SqlHelper https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/3705831607/Sql+Integration).
  That will allow you to work with the database, temp mail and temp phone services, and with all features in our Helper Project like SeleniumWrapper or BaseRequest.
   
* Database configuration
  To work with DB enter the required credentials in the env.runsettings.
  
* How to run tests    
  - Enter required data in env.runsettings if you have to use any credentials or configurations.
  - Choose x64 launch platform  in menu Test/Processor Architecture for AnyCPU projects.
  - Choose runsettings in menu Test/Configure Run Settings/Select Solution Wide runsettings File. 
  - Find the test in Test Explorer and run it. 
  
* Deployment instructions
  Repository also has 
  - RunTC.cmd file in case if you need to run your tests on CI-CD (instruction for RunTC.cmd https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/1198786024/RunTC.cmd).
  - class AllureHooks that craetes Allure reports after test run. (about allure https://maxiproject.atlassian.net/wiki/spaces/WAP/pages/1511096757/Allure+Reports)
  To generate allure reports localy 
   - open in folder ATDemo\ATFramework\ATFramework\bin\Debug\netcoreapp3.1 cmd and use comands 
   - or use allure.bat serve .\allure-results (this coman immediately opens report)
   - or use allure.bat generate .\allure-results -o .\allure-results\html (after -o is the path where you want to save the generated file,
   but the saved report does not open in Chrome, you need to either open attachments or open through Firefox)
  
### Contribution guidelines ###

* Writing tests
  General structure to start writing your own tests is already exist. You will find some examples of writing Feature files and step usage in scenarios. 
  You also find example of usage CAS (Configurable Api Steps https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/3157885648/Configurable+Api+Steps ). This
  feature will help you make all kind of API requests in your tests.
  
  General flow of writing automation tests:
  - Write Feature file 
  - Create POM.
  - Generat and implement steps.  
  - Create API methods (or you can use CAS).
  - Create and implement interface (Base Info for Create Interface https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/2521661472/Base+Info+for+Create+Interface).
  - Run an autotest and set the @Automated_Test tag.
      
  Also how to use SQL steps - Rules to use sql steps https://maxiproject.atlassian.net/wiki/spaces/CRM/pages/1499038020/Rules+to+use+sql+steps
	
* Other guidelines
  

### Who do I talk to? ###

P.S. For questions and feedbacks please contact the Automation team.